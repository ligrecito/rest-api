FORMAT: 1A

# Maniflexx Device API

The set of APIs used by native devices.

### /device/employees

#### Retrieve employee details [GET]

Retrieves the details of the authenticated employee.

**Roles**
- ROLE_ONSITE_TEAML
- ROLE_ONSITE_UNLOAD

**Pre-conditions**
- User is logged in

+ Request (application/json)

    + Headers

            Accept: application/vnd.maniflexx.employee.v1+json
            Authorization: Bearer <JWT>

    + Body

+ Response 200 (application/vnd.maniflexx.employee.v1+json)

    The employee object with the details of the authenticated employee.

    + Body

            {
              "username": "adam",
              "employeeId": "abcde-fghi-jklm-1234",
              "employeeName": "Adam Sandle",
              "roles": [
                "ROLE_ONSITE_TEAML"
              ],
              "employerId": "12345-6789-0123-abcd",
              "employerName": "Containers r' us",
              "siteName": "Wonderland Dr Eastern Creek"
            }

    + Schema

            {
              "type": "object",
              "properties": {
                "username": {
                  "type": "string",
                  "description": "The username that the employee authenticated with."
                },
                "employeeId": {
                  "type": "string",
                  "description": "The unique identifier of the employee."
                },
                "employeeName": {
                  "type": "string",
                  "description": "The full name of the employee."
                },
                "roles": {
                  "type": "array",
                  "items": {
                    "type": "string",
                    "enum": [
                      "ROLE_ONSITE_TEAML",
                      "ROLE_ONSITE_UNLOAD"
                    ]
                  },
                  "description": "The array of roles assigned to the employee."
                },
                "employerId": {
                  "type": "string",
                  "description": "The unique identifier of the employee's employer."
                },
                "employerName": {
                  "type": "string",
                  "description": "The name of the employee's employer."
                },
                "siteName": {
                  "type": "string",
                  "description": "The name of the site at which the employee is currently working."
                }
              }
            }

+ Request (application/json)

    + Headers

            Accept: application/vnd.maniflexx.employee.v1+json
            Authorization: Bearer <JWT>

    + Body

+ Response 401 (application/vnd.maniflexx.employee.v1+json)

    The authenticated user is not in either the team leader or unloader roles.

    + Body

### /device/manifests

#### Retrieve manifests [GET]

Gets all manifests the currently logged in user needs to view or manage. A team leader should see containers in the "unloading" status even if they were not the one assigned to unload them, as these appear on the main manifest view under the "in progress" filter.

If a container in the manifest has not been assigned, the *assignedTo* collection object will be the empty set.

If a container in the manifest has been assigned for unloading, the *assignedTo* collection will contain a list of employee ID employee name pairs.

**Roles**
- ROLE_ONSITE_TEAML

**Pre-conditions**
- User is logged in

**Post-conditions**
- The manifests and the containers within are sorted in the order they should appear on the screen.

+ Request (application/json)

    + Headers

            Accept: application/vnd.maniflexx.manifest.collection.v1+json
            Authorization: Bearer <JWT>

    + Body

+ Response 200 (application/vnd.maniflexx.manifest.collection.v1+json)

    The list of manifests assigned to the team leader for managing.

    + Body

            [
              {
                "meta": {
                  "containerNumber": "CMAU5131034",
                  "supplierCode": "SNE004",
                  "poContainerNumber": "S28152",
                  "orderNumber": "28152",
                  "performance": null,
                  "sealed": true,
                  "status": "ASSIGNED",
                  "assignedTo": [
                    {
                      "employeeId": "12345-6789-0123-abcd",
                      "employeeName": "Adam Sandle"
                    },
                    {
                      "employeeId": "abcde-fghi-jklm-1234",
                      "employeeName": "Bob Smith"
                    }
                  ]
                },
                "complexity": {
                  "numSkus": 11,
                  "countSkus": 444,
                  "complexityValue": 4884,
                  "complexity": "MEDIUM"
                },
                "items": [
                  {
                    "sku": "56216",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "48",
                      "description": "Water Splash Flying Disk 17cm Pk1 Asstd",
                      "outersOnOrder": "88"
                    }
                  },
                  {
                    "sku": "56058",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "24",
                      "description": "Water Gun 22cm Asstd Cols",
                      "outersOnOrder": "150"
                    }
                  },
                  {
                    "sku": "56059",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "48",
                      "description": "Water Splash Fish Missle 16cm Pk1 Asstd",
                      "outersOnOrder": "88"
                    }
                  },
                  {
                    "sku": "56052",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "12",
                      "description": "Beach Set 6 Pc Round Bucket with Sifter Play Set",
                      "outersOnOrder": "300"
                    }
                  },
                  {
                    "sku": "56053",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "12",
                      "description": "Beach Set 7 Pc Square Bucket Dig and Play",
                      "outersOnOrder": "300"
                    }
                  },
                  {
                    "sku": "56054",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "72",
                      "description": "Water Gun 12cm Animals Asstd Designs",
                      "outersOnOrder": "59"
                    }
                  },
                  {
                    "sku": "56055",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "72",
                      "description": "Flying Disk Set 3 Small 85-125mm",
                      "outersOnOrder": "67"
                    }
                  },
                  {
                    "sku": "56060",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "48",
                      "description": "Water Splash Balls 7cm Pk2 Asstd Sport Ball Designs",
                      "outersOnOrder": "97"
                    }
                  },
                  {
                    "sku": "56061",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "48",
                      "description": "Water Splash Grenade 11cm Pk2",
                      "outersOnOrder": "88"
                    }
                  },
                  {
                    "sku": "56062",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "48",
                      "description": "Water Splash Ball Football 16cm Pk1",
                      "outersOnOrder": "88"
                    }
                  },
                  {
                    "sku": "56051",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "12",
                      "description": "Beach Set 3 Pc Watering Can Spade Rake",
                      "outersOnOrder": "400"
                    }
                  }
                ]
              }
            ]

    + Schema

            {
              "type": "object",
              "properties": {
                "meta": {
                  "type": "object",
                  "properties": {
                    "containerNumber": {
                      "type": "string",
                      "description": "The unique identifier of the container."
                    },
                    "supplierCode": {
                      "type": "string"
                    },
                    "poContainerNumber": {
                      "type": "string"
                    },
                    "orderNumber": {
                      "type": "string"
                    },
                    "performance": {
                      "type": "integer"
                    },
                    "sealed": {
                      "type": "boolean"
                    },
                    "status": {
                      "type": "string",
                      "description": "The current state of the container.",
                      "enum": [
                        "UNASSIGNED",
                        "SCHEDULED",
                        "ASSIGNED",
                        "UNLOADING",
                        "COMPLETED"
                      ]
                    },
                    "assignedTo": {
                      "type": "array",
                      "description": "The list of employees assigned to unload the container.",
                      "items": {
                        "type": "object",
                        "properties": {
                          "employeeId": {
                            "type": "string",
                            "description": "The unique identifier of the employee."
                          },
                          "employeeName": {
                            "type": "string",
                            "description": "The full name of the employee."
                          }
                        }
                      }
                    }
                  }
                },
                "complexity": {
                  "type": "object",
                  "properties": {
                    "numSkus": {
                      "type": "integer",
                      "description": "The number of distinct SKUs located in the container."
                    },
                    "countSkus": {
                      "type": "integer",
                      "description": "The total number of items in the container."
                    },
                    "complexityValue": {
                      "type": "integer",
                      "description": "A numerical representation of the difficulty of unloading the container."
                    },
                    "complexity": {
                      "type": "string",
                      "description": "The classification of the difficulty of unloading the container.",
                      "enum": [
                        "EASY",
                        "MEDIUM",
                        "HARD"
                      ]
                    }
                  }
                },
                "items": {
                  "type": "array",
                  "description": "The list of items in the container.",
                  "items": {
                    "type": "object"
                  }
                }
              }
            }

+ Request (application/json)

    + Headers

            Accept: application/vnd.maniflexx.manifest.collection.v1+json
            Authorization: Bearer <JWT>

    + Body

+ Response 401 (application/vnd.maniflexx.manifest.collection.v1+json)

    The authenticated user is not in the team leader role.

    + Body

### /device/teams

#### Retrieve team members [GET]

Gets the set of employees able to be assigned to containers by the currently logged in team leader, plus a set of groups of employees who have previously been assigned together. If an employee is not available to be assigned today but was previously assigned to a team, that team should not appear in the teams collection.

**Roles**
- ROLE_ONSITE_TEAML

**Pre-conditions**
- User is logged in

**Post-conditions**
- The employees and teams collections should be sorted alphabetically by employee name.

+ Request (application/json)

    + Headers

            Accept: application/vnd.maniflexx.teams.collection.v1+json
            Authorization: Bearer <JWT>

    + Body

+ Response 200 (application/vnd.maniflexx.teams.collection.v1+json)

    The list of employees and employee teams available for assignment to unload a container.

    + Body

            {
              "employees": [
                {
                  "employeeId": "12345678",
                  "employeeName": "Adam Sandle "
                },
                {
                  "employeeId": "23456789",
                  "employeeName": "Bob Smith "
                },
                {
                  "employeeId": "34567890",
                  "employeeName": "Jack Jackson "
                }
              ],
              "teams": [
                [
                  {
                    "employeeId": "12345678",
                    "employeeName": "Adam Sandle"
                  },
                  {
                    "employeeId": "23456789",
                    "Bob Smith": null
                  }
                ],
                [
                  {
                    "employeeId": "12345678",
                    "employeeName": "Adam Sandle"
                  },
                  {
                    "employeeId": "34567890",
                    "Jack Jackson": null
                  }
                ]
              ]
            }

    + Schema

            {
              "type": "object",
              "properties": {
                "employees": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "employeeId": {
                        "type": "string",
                        "description": "The unique identifier of the employee."
                      },
                      "employeeName": {
                        "type": "string",
                        "description": "The full name of the employee."
                      }
                    }
                  }
                },
                "teams": {
                  "type": "array",
                  "items": {
                    "type": "array",
                    "items": {
                      "type": "object",
                      "properties": {
                        "employeeId": {
                          "type": "string",
                          "description": "The unique identifier of the employee."
                        },
                        "employeeName": {
                          "type": "string",
                          "description": "The full name of the employee."
                        }
                      }
                    }
                  }
                }
              }
            }

+ Request (application/json)

    + Headers

            Accept: application/vnd.maniflexx.teams.collection.v1+json
            Authorization: Bearer <JWT>

    + Body

+ Response 401 (application/vnd.maniflexx.teams.collection.v1+json)

    The authenticated user is not in the team leader role.

    + Body

### /device/containers

#### Retrieve assigned containers [GET]

Gets all containers assigned to the currently logged in user for unloading. Unlike the /device/manifests service, team leaders calling this service would only see containers in the "assigned" status they themselves are assigned to unload, as these are presented on a different tab view.

**Roles**
- ROLE_ONSITE_TEAML
- ROLE_ONSITE_UNLOAD

**Pre-conditions**
- User is logged in

**Post-conditions**
- The containers are sorted in the order they should appear on the screen.

+ Request (application/json)

    + Headers

            Accept: application/vnd.maniflexx.manifest.collection.v1+json
            Authorization: Bearer <JWT>

    + Body

+ Response 200 (application/vnd.maniflexx.manifest.collection.v1+json)

    The list of manifests assigned to the team leader for managing.

    + Body

            [
              {
                "meta": {
                  "containerNumber": "CMAU5131034",
                  "supplierCode": "SNE004",
                  "poContainerNumber": "S28152",
                  "orderNumber": "28152",
                  "performance": null,
                  "sealed": true,
                  "status": "ASSIGNED",
                  "assignedTo": [
                    {
                      "employeeId": "12345-6789-0123-abcd",
                      "employeeName": "Adam Sandle"
                    },
                    {
                      "employeeId": "abcde-fghi-jklm-1234",
                      "employeeName": "Bob Smith"
                    }
                  ]
                },
                "complexity": {
                  "numSkus": 11,
                  "countSkus": 444,
                  "complexityValue": 4884,
                  "complexity": "MEDIUM"
                },
                "items": [
                  {
                    "sku": "56216",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "48",
                      "description": "Water Splash Flying Disk 17cm Pk1 Asstd",
                      "outersOnOrder": "88"
                    }
                  },
                  {
                    "sku": "56058",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "24",
                      "description": "Water Gun 22cm Asstd Cols",
                      "outersOnOrder": "150"
                    }
                  },
                  {
                    "sku": "56059",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "48",
                      "description": "Water Splash Fish Missle 16cm Pk1 Asstd",
                      "outersOnOrder": "88"
                    }
                  },
                  {
                    "sku": "56052",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "12",
                      "description": "Beach Set 6 Pc Round Bucket with Sifter Play Set",
                      "outersOnOrder": "300"
                    }
                  },
                  {
                    "sku": "56053",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "12",
                      "description": "Beach Set 7 Pc Square Bucket Dig and Play",
                      "outersOnOrder": "300"
                    }
                  },
                  {
                    "sku": "56054",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "72",
                      "description": "Water Gun 12cm Animals Asstd Designs",
                      "outersOnOrder": "59"
                    }
                  },
                  {
                    "sku": "56055",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "72",
                      "description": "Flying Disk Set 3 Small 85-125mm",
                      "outersOnOrder": "67"
                    }
                  },
                  {
                    "sku": "56060",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "48",
                      "description": "Water Splash Balls 7cm Pk2 Asstd Sport Ball Designs",
                      "outersOnOrder": "97"
                    }
                  },
                  {
                    "sku": "56061",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "48",
                      "description": "Water Splash Grenade 11cm Pk2",
                      "outersOnOrder": "88"
                    }
                  },
                  {
                    "sku": "56062",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "48",
                      "description": "Water Splash Ball Football 16cm Pk1",
                      "outersOnOrder": "88"
                    }
                  },
                  {
                    "sku": "56051",
                    "content": {
                      "palletsOnOrder": "1",
                      "cartonsPerLayer": "0",
                      "inners": "12",
                      "maxLayers": "0",
                      "outers": "12",
                      "description": "Beach Set 3 Pc Watering Can Spade Rake",
                      "outersOnOrder": "400"
                    }
                  }
                ]
              }
            ]

    + Schema

            {
              "type": "object",
              "properties": {
                "meta": {
                  "type": "object",
                  "properties": {
                    "containerNumber": {
                      "type": "string",
                      "description": "The unique identifier of the container."
                    },
                    "supplierCode": {
                      "type": "string"
                    },
                    "poContainerNumber": {
                      "type": "string"
                    },
                    "orderNumber": {
                      "type": "string"
                    },
                    "performance": {
                      "type": "integer"
                    },
                    "sealed": {
                      "type": "boolean"
                    },
                    "status": {
                      "type": "string",
                      "description": "The current state of the container.",
                      "enum": [
                        "UNASSIGNED",
                        "SCHEDULED",
                        "ASSIGNED",
                        "UNLOADING",
                        "COMPLETED"
                      ]
                    },
                    "assignedTo": {
                      "type": "array",
                      "description": "The list of employees assigned to unload the container.",
                      "items": {
                        "type": "object",
                        "properties": {
                          "employeeId": {
                            "type": "string",
                            "description": "The unique identifier of the employee."
                          },
                          "employeeName": {
                            "type": "string",
                            "description": "The full name of the employee."
                          }
                        }
                      }
                    }
                  }
                },
                "complexity": {
                  "type": "object",
                  "properties": {
                    "numSkus": {
                      "type": "integer",
                      "description": "The number of distinct SKUs located in the container."
                    },
                    "countSkus": {
                      "type": "integer",
                      "description": "The total number of items in the container."
                    },
                    "complexityValue": {
                      "type": "integer",
                      "description": "A numerical representation of the difficulty of unloading the container."
                    },
                    "complexity": {
                      "type": "string",
                      "description": "The classification of the difficulty of unloading the container.",
                      "enum": [
                        "EASY",
                        "MEDIUM",
                        "HARD"
                      ]
                    }
                  }
                },
                "items": {
                  "type": "array",
                  "description": "The list of items in the container.",
                  "items": {
                    "type": "object"
                  }
                }
              }
            }

+ Request (application/json)

    + Headers

            Accept: application/vnd.maniflexx.manifest.collection.v1+json
            Authorization: Bearer <JWT>

    + Body

+ Response 401 (application/vnd.maniflexx.manifest.collection.v1+json)

    The authenticated user is not in either the team leader or unloader roles.

    + Body

### /device/containers/{containerNumber}/assign

#### Assign team to container [POST]

Assigns the given set of employees to unload the container.

**Roles**
- ROLE_ONSITE_TEAML

**Pre-conditions**
- User is logged in
- The container is in the SCHEDULED state

**Post-conditions**
- The given employees are assigned to the container
- The container is set to the ASSIGNED state
- The set of employees assigned to the container will appear as a pre-defined team in the /device/teams API response.

+ Parameters

    + containerNumber (required)

+ Request (application/vnd.maniflexx.teams.collection.v1+json)

    + Headers

            Accept: application/json
            Authorization: Bearer <JWT>

    + Body

            [
                "12345678",
                "23456789"
            ]

    + Schema

            {
              "type": "array",
              "items": {
                "type": "string",
                "description": "The unique identifier of the employee.",
                "example": [
                  "12345678",
                  "23456789"
                ]
              }
            }

+ Response 200 (application/json)

    The employees were successfully assigned to the container.

    + Body

+ Request (application/vnd.maniflexx.teams.collection.v1+json)

    + Headers

            Accept: application/json
            Authorization: Bearer <JWT>

    + Body

            [
                "12345678",
                "23456789"
            ]

    + Schema

            {
              "type": "array",
              "items": {
                "type": "string",
                "description": "The unique identifier of the employee.",
                "example": [
                  "12345678",
                  "23456789"
                ]
              }
            }

+ Response 400 (application/json)

    The container is not in the SCHEDULED state.

    + Body

+ Request (application/vnd.maniflexx.teams.collection.v1+json)

    + Headers

            Accept: application/json
            Authorization: Bearer <JWT>

    + Body

            [
                "12345678",
                "23456789"
            ]

    + Schema

            {
              "type": "array",
              "items": {
                "type": "string",
                "description": "The unique identifier of the employee.",
                "example": [
                  "12345678",
                  "23456789"
                ]
              }
            }

+ Response 401 (application/json)

    The authenticated user is not in the team leader role.

    + Body

+ Request (application/vnd.maniflexx.teams.collection.v1+json)

    + Headers

            Accept: application/json
            Authorization: Bearer <JWT>

    + Body

            [
                "12345678",
                "23456789"
            ]

    + Schema

            {
              "type": "array",
              "items": {
                "type": "string",
                "description": "The unique identifier of the employee.",
                "example": [
                  "12345678",
                  "23456789"
                ]
              }
            }

+ Response 404 (application/json)

    The container does not exist.

    + Body

### /device/containers/{containerNumber}/unseal

#### Unseal container [POST]

Marks the container as unsealed.

**Roles**
- ROLE_ONSITE_TEAML
- ROLE_ONSITE_UNLOAD

**Pre-conditions**
- User is logged in
- User is currently assigned to the given container
- The container is in the ASSIGNED state

**Post-conditions**
- The container is set to the UNLOADING state

+ Parameters

    + containerNumber (required)

+ Request (application/json)

    + Headers

            Accept: application/json
            Authorization: Bearer <JWT>

    + Body

+ Response 200 (application/json)

    The container was successfully unsealed.

    + Body

+ Request (application/json)

    + Headers

            Accept: application/json
            Authorization: Bearer <JWT>

    + Body

+ Response 400 (application/json)

    The container is not in the ASSIGNED state.

    + Body

+ Request (application/json)

    + Headers

            Accept: application/json
            Authorization: Bearer <JWT>

    + Body

+ Response 401 (application/json)

    The authenticated user is not in either the team leader or unloader roles.

    + Body

+ Request (application/json)

    + Headers

            Accept: application/json
            Authorization: Bearer <JWT>

    + Body

+ Response 404 (application/json)

    The container does not exist.

    + Body

### /device/containers/{containerNumber}/complete

#### Complete container unloading [POST]

Mark the container as completed to signify it has been fully unloaded.

**Roles**
- ROLE_ONSITE_TEAML
- ROLE_ONSITE_UNLOAD

**Pre-conditions**
- User is logged in
- User is currently assigned to the given container
- The container is in the UNLOADING state

**Post-conditions**
- The container is set to the COMPLETED state

+ Parameters

    + containerNumber (required)

+ Request (application/json)

    + Headers

            Accept: application/json
            Authorization: Bearer <JWT>

    + Body

+ Response 200 (application/json)

    The container was successfully completed.

    + Body

+ Request (application/json)

    + Headers

            Accept: application/json
            Authorization: Bearer <JWT>

    + Body

+ Response 400 (application/json)

    The container is not in the UNLOADING state.

    + Body

+ Request (application/json)

    + Headers

            Accept: application/json
            Authorization: Bearer <JWT>

    + Body

+ Response 401 (application/json)

    The authenticated user is not in either the team leader or unloader roles.

    + Body

+ Request (application/json)

    + Headers

            Accept: application/json
            Authorization: Bearer <JWT>

    + Body

+ Response 404 (application/json)

    The container does not exist.

    + Body


