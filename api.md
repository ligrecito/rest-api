
# REST API Reference

## Base URL

All URLs referenced in the documentation have the following base:

```
http://api.maniflexx.com:8080
```

## Authentication

HTTP requests to the REST API are protected. In short, all users will have to request a security token using STS endpoint.
This token will be passed in all subsequent calls in the authorization header as follows:

```
Authorization: Bearer <JWT_TOKEN_HERE>
```

## Security Roles

Permission model implemented in this API is via a rudimentary Role Based Access Control (RBAC). The following roles have been defined:

 - `ROLE_WEB_ADMIN`: Access to all endpoints related to webapp.
 - `ROLE_WEB_DASHBOARD`: Access to only endpoints related to Dashboard (view) of the webapp
 - `ROLE_ONSITE_TEAML`: Access to all endpoints called by the team leader version of the native app
 - `ROLE_ONSITE_UNLOAD`: Access to all endpoints called by the unloader version of the native app

These roles will be part of the JWT Token in the claim with the obscure name of "*roles*".

Following is an example of the payload section of the JWT Token.

 ```
{
  "sub": "keith",
  "roles": "ROLE_WEB_ADMIN",
  "iss": "myself",
  "exp": 1471257177
}
 ```
Keep in mind that an user can have multiple roles. And those roles are groupped together under a profile name.

## Dates and Times

All dates and times will be represented following the standard: `ISO 8601`.

- Dates: `yyyy-MM-dd`
- DateTime: `yyyy-MM-ddTHH:mm:ss`


### GET /api/roles

Get all available roles in the system

#### Response

```
{
  "roles": [
    "ROLE_ONSITE_UNLOAD",
    "ROLE_ONSITE_TEAML",
    "ROLE_WEB_DASHBOARD",
    "ROLE_WEB_ADMIN"
  ]
}
```

### GET /api/profiles/companies/{cid}

Return all the profiles available for a given company: `cid`

#### Response

```
{
  "profiles": [
    {"profileId": 1, "profileName": "web admin", "roles": ["ROLE_WEB_ADMIN"]},
    {"profileId": 2, "profileName": "team principal", "roles": ["ROLE_WEB_DASHBOARD", "ROLE_ONSITE_TEAML"]},
    {"profileId": 3, "profileName": "container unloader", "roles": ["ROLE_ONSITE_UNLOAD"]},

  ]
}
```

### POST /api/profiles/companies/{cid}/

Service not implemented

#### Request
- RequestBody

```
{
    {"profileName": "web admin", "roles": ["ROLE_WEB_ADMIN"]}
}
```

### DELETE /api/profiles?name={companyName}&id={companyId}&profile=profile

Remove a given profile id `profile` for a company id: `companyid` or company name: `companyName`

If company name is passed then it takes priority, this means companyId is ignored.
If company name is not passed then company id must be provided.
If both company name and id are not passed then HTTP 422 Unprocessable entity is returned.

#### Request

- profile - name of profile to be deleted, name case insensitive (service calls toUpperCase for profile)
- companyId - the unique company identifier the profile is deleted from
- companyName - the name of company profile is deleted from. The name is case sensitive, it must exactly match its db value.

#### Response 

- HTTP 200 OK, true if profile has been deleted
- HTTP 200 OK, false if profile to delete was not found or company has not been found
- HTTP 422 Unprocessable Entity - when company id and company name are missing in request

### PUT /api/profiles/{pid}/employees/{id}

Assign all the roles associated to profile id: `pid` to the employee `id`




## Users

For developing purposes the following users have been created:

 **WebApp user (ROLE: ROLE_WEB_ADMIN)**

 - username: `sally`, password: `secret` 

 **Onsite user (ROLE: ROLE_ONSITE_TEAML)**

 - username: `adam`, password: `secret` 

 **Onsite user (ROLE: ROLE_ONSITE_UNLOAD)**
 
 - username: `bob`, password: `secret` 

# Resource STS 

Operations related to the security token (JWT).

###  POST /sts/issue

Request for a new JWT token. This token will live for 1 hour, hence your code will have to refresh this token before hand.

#### Request

- RequestBody:
    ```
    { "username": String,
      "password": String}
    ```

### POST /sts/refresh

Refresh a token. Keep in mind that only valid tokens can be refreshed.

#### Request

- RequestBody:
    ```
    { "token": String }
    ```


# Resource Arrangements

Contractual agreements between companies. Generally between retail companies and unloading ones.

### GET /api/arrangements

Retrieve all arrangements recorded in the system

### GET /api/arrangements/{id}

Get detailed information about a given arrangement


# Resource Companies

### GET /api/companies

TODO: Service not implemented 
Get all companies registered in the system

### GET /api/companies/{companyId}/query?fields=value1&..&fields=valueN

Get company details for given companyId. Service accepts list of fields for filtering data, this means only the attributes that are listed as fileds will be returned.

#### Request

- companyId - unique identifier of company to be returned (PK ObjectId in database). Mandatory.
- fields - array of company attributes to be filtered. Following values are allowed: id, type, name, contact, contracts, employees, site, profiles, activities. If unknown value is passed then thie filed will have following value returned: "** Error: Field does not exist."

#### Response

- HTTP 200 OK, company details, only attributes listed in fields array will be returned.
- HTTP 404 Not found - when company details has not been found in database

### GET /api/companies?name={name}

TODO: Service not implemented
Get company details given a company name

### GET /api/companies/{id}/arrangements

TODO: Service not implemented
Get information about all the companies that the given company has arrangements with.

### GET /api/companies/{id}/roles

TODO: Service not implemented
Retrieve all the roles and permissions (scopes) defined for a given company

### GET /api/companies/{companyId}/contracts

TODO: update details

### GET /api/companies/{companyId}/activity?requestorId=requestorId?start=startDate?end=endDate

| Role Names |
| - | - |
| ROLE_WEB_ADMIN | ROLE_WEB_DASHBOARD | 

Returns list of activities for specified companyId and optional parameters. Service will return empty array for invalid search parameters.

#### Request

- companyId - unloading company id, required
- requestorId - retail company id, optional
- start - start date of the range for unloading due date activities should be returned. Optional, if not provided current date is used
- end - end date of the range for unloading due date activities should be returned. Optional, if not provided current date is used

#### Response

List of activities or empty array [] if no activities found.

```
[
  {
    "activityId": "589871b92a19958960fbb8a1",
    "companyId": "589871b92a19958960fbb89a",
    "requestorCompanyId": "589871b82a19958960fbb892",
    "requestorCompanyName": "Kmart",
    "unloadingDueDate": "2017-02-06",
    "employeesDeployedOnSite": null,
    "shippingManifest": [
      {
        "containerNumber": "MRKU9727355",
        "sealed": true,
        "status": "UNSCHEDULED",
        "performance": null,
        "assignedTo": [],
        "meta": {
          "orderNumber": "28648",
          "poContainerNumber": "S28648",
          "supplierCode": "SAP001"
        },
        "items": [
          {
            "sku": "52416",
            "attributes": {
              "palletsOnOrder": "13",
              "cartonsPerLayer": "12",
              "inners": "12",
              "maxLayers": "3",
              "outers": "48",
              "description": "Notebook Card Cover Basic A4 120pg",
              "outersOnOrder": "450"
            }
          }, ...
        ],
        "complexity": {
          "numSkus": 1,
          "countSkus": 48,
          "complexityValue": 48,
          "complexity": "LOW"
        }
      }
    }, ...
   ]
  }
]
          
```

### GET /api/companies/workforce

TODO: update details

### POST /api/companies/upload

TODO: update details

### GET /api/companies/{companyId}/metrics

TODO: add date conditions that allow narrowing reporting period.

Returns various metrics related to container processing for specific company. Service doesn't check if company exists. It only reacts if activities for company exists or not. 

#### Request

- companyId - the PK of company metrics are requested for

#### Response

- HTTP 200 OK when company activities found
```
{
  "totalContainers": 6,          // total number of containers regardless of its status
  "assignedContainers": 1,       // number of containers in ASSIGNED status
  "inProgressContainers": 0,     // number of containers in UNLOADING status
  "completedSuccess": 0,         // number of containers in COMPLETED status without any errors
  "completedErrors": 0,          // number of containers in COMPLETED status with errors recorded
  "totalUnloadingTime": 0,       // total unloading time - for COMPLETED contaners only
  "avgUnloadingPerContainer": 0, // average unloading time for COMPLETED containers only
  "errors": [                    // list of error information for any containers that are assigned to company with recorded issues.
     {"containerNumber": "AA1", files: ["INCFile1a", "INCFile2a"] },
     {"containerNumber": "AA2", files: ["INCFile1b", "INCFile2b"] }
  ]     
}
```
- HTTP 404 Not Found if no activities found.


### POST /api/companies

Creates a new company with basic information: name, contact details and default profiles.

- There is no check if company name is unique
- Contract is created between new company and logged in user's company
- default profiles are added to unloading company only

#### Roles

| Role Names |
| - | - |
| ROLE_WEB_ADMIN | ROLE_WEB_DASHBOARD | 

#### Request

```
{
    name: String,     // company name, required
    type: String,     // unloading (unloading company) or retail (Retail Company), required
    email: String,    // required
    website: String,  // optional
    phone: String     // optional
}
```

#### Response

At this stage profiles are not returned.

- HTTP 200 OK

```
{
  "id": "587cba88e0f02b2fadd5559f",
  "name": "Company name",
  "type": "retail",
  "website": "www.w.com",
  "email": "c@c.com",
  "phone": "23423423"
}
```

- HTTP 422 Unprocesseable entity - when for any reason company can't be created.
- HTTP 400 Bad Request - when invalid data (type not in retail or unloading) passed 

### POST /api/companies/{companyId}/employee

| Role Names |
| - | - |
| ROLE_WEB_ADMIN | ROLE_WEB_DASHBOARD |


Creates employee details and assigns it to specified company.

#### Pre conditions

- Company with id=companyId exists in database
- Employee doesnt exist in database. If employee with the same username exists request will fail.

#### Post conditions

- New record for employee has been created.
- New employee has been added to company employees.

#### Request

- companyId - unique identifier of the company that employee is added to.
- employee details:
- - full name
- - username - the username used for login
- - password - plain text, it will be encoded.
- - email
- - phone
- - list of role names 


#### Response

- HTTP 200 OK, returns employee details that has been created including unique eployee id.
- HTTP 404 Not found - company not found. no additional data returned
- HTTP 422 Unprocessable Entity, for example:

```
{
  "code": "ResourceAlreadyExistsException",
  "message": "<bob> already exists"
}
```

### DELETE /api/companies/{companyId}/employee/{employeeId}

| Role Names |
| - | - | - | - |
| ROLE_WEB_ADMIN | ROLE_WEB_DASHBOARD | ROLE_ONSITE_UNLOAD | ROLE_ONSITE_TEAML |

Removes specified employee from specified company. It removes employee record from array of employees of specified company. 
This service doesn't delete employee details. Also Employee.employedBy contains record. (TODO is there any value of keeping it?).

#### Pre conditions:

- Employee is assinged to company
- Employee can only be assigned to one company

#### Post conditions:

- Employee is removed from company employees
- Employee exists is Employees collection.

#### Request

- companyId - unique identifier of company that employee is deleted from. Mandatory.
- employeeId - unique identifier of employee that is removed from company. Mandatory.

#### Response

- HTTP 200 OK, employee has been removed from company.
- HTTP 404 Not found, company or employee has not been found.

### GET /api/companies/{unloadingCompanyId}/shifts

Returns list of shifts defined for unloading company. Shifts only makes sense for unloading company.

#### Roles

| Role Names |
| - | - | - |
| ROLE_WEB_ADMIN | ROLE_ONSITE_TEAML |ROLE_ONSITE_UNLOAD | 

#### Pre conditions

- Unloading company is defined in the system

#### Post conditions

- shift provided in input is added to company shifts

#### Request

- unloadingCompanyId - PK of unloading company

#### Response

If company has no shifts defined or unloadingCompanyId provided is not defined then empty array [] is returned.

HTTP 200 - OK

```
[
  {
    "shiftStart": "00:00",
    "shiftEnd": "08:00"
  },
  {
    "shiftStart": "08:00",
    "shiftEnd": "16:00"
  }

]
```

### POST /api/companies/{unloadingCompanyId}/shifts

Adds shift for unloading company.

#### Roles

| Role Names |
| - | - | - |
| ROLE_WEB_ADMIN | ROLE_ONSITE_TEAML |ROLE_ONSITE_UNLOAD | 

#### Pre conditions

- Unloading company is defined in the system

#### Post conditions

- shift provided in input is added to company shifts

#### Request

- unloadingCompanyId - PK of unloading company

```
  {
    "shiftStart": "00:00",
    "shiftEnd": "08:00"
  }
  
```

#### Response

- HTTP 200 OK if shift has been added to company and shift details that has been added.

```
  {
    "shiftStart": "00:00",
    "shiftEnd": "08:00"
  }
  
```

- HTTP 404 Not Found if company identified by unlodingCompanyId has not been found.

### DELETE /api/companies/{unloadingCompanyId}/shifts

Delete specified shift from unloading company shifts. 

#### Roles

| Role Names |
| - | - | - |
| ROLE_WEB_ADMIN | ROLE_ONSITE_TEAML |ROLE_ONSITE_UNLOAD | 

#### Pre conditions

- Unloading company is defined in the system and has shift to be deleted defined

#### Post conditions

- shift provided in input is removed from company shifts

#### Request

- unloadingCompanyId - PK of unloading company

```
[
  {
    "shiftStart": "00:00",
    "shiftEnd": "08:00"
  }
]
```

#### Response

- HTTP 200 OK - if shift has been deleted
- HTTP 404 Not found if unloading company has not been found

# Resource Containers

Resource representing a shipping container. The content of each container will be described in
a manifest.

### PUT /api/container/{containerNumber}/unseal

Mark a container as unsealed. Service doesn't check if container is already unsealed. The user that is logged in and calling unseal must be assigned to the container.

#### Pre conditions

- user must be logged in to maniflexx
- user must be assigned to the container to be unsealed
- container must be assigned to company employing user

#### Post conditions

- container sealed flag is changed to FALSE
- container status is changed to UNLOADING
- container performance field is instantiated with current time so unloading time may be later calculated.

#### Request

- containerNumber - container number to be unsealed

#### Response

- HTTP 200 OK, if at container updated.
- HTTP 404 Not found if container with containerId doesn't exists.

#### Notes

- Not sure if sealed flag is needed. So far changing container status to UNLOADING is the same. 

### PUT /api/container/{containerNumber}/completed

Marks container as unloading completed.

#### Pre confitions

- user must me logged in to the system
- container must be assigned to company who is employing user that is logged in and called service.

#### Post conditions

- container's status is updated to COMPLETED
- container's performance field is updated with time taken from unseal to complete. (Time in miliseconds.)

#### Request

- containerNumber - the number of container that unloading has been completed.

#### Response

- HTTP 200 OK - container marked as completed.
- HTTP 404 Not Found - container not found.

### GET /api/container/assignment

Return list of containers assigned to employee that is logged in and called this service.

#### Request

- no extra parameters

#### Response

- HTTP 200 OK

Sample response:
```
[
  {
    "meta": {
      "containerNumber": "CMAU5131034",
      "supplierCode": "SNE004",
      "poContainerNumber": "S28152",
      "orderNumber": "28152",
      "performance": null,
      "sealed": true,
      "status": "ASSIGNED",
      "assignedTo": [
        {
          "timestamp": 1480147117,
          "machineIdentifier": 7903099,
          "processIdentifier": 7906,
          "counter": 7062073,
          "time": 1480147117000,
          "date": "2016-11-26T07:58:37.000+0000",
          "timeSecond": 1480147117
        }
      ]
    },
    "complexity": {
      "numSkus": 11,
      "countSkus": 444,
      "complexityValue": 4884,
      "complexity": "MEDIUM"
    },
    "items": [
     {
        "sku": "56058",
        "content": {
          "palletsOnOrder": "1",
          "cartonsPerLayer": "0",
          "inners": "12",
          "maxLayers": "0",
          "outers": "24",
          "description": "Water Gun 22cm Asstd Cols",
          "outersOnOrder": "150"
        }
      },
      {
        "sku": "56059",
        "content": {
          "palletsOnOrder": "1",
          "cartonsPerLayer": "0",
          "inners": "12",
          "maxLayers": "0",
          "outers": "48",
          "description": "Water Splash Fish Missle 16cm Pk1 Asstd",
          "outersOnOrder": "88"
        }
      }
    ]
  }
]
```

- HTTP 404 Not Found if no assigned containers found

### POST /api/container/assignment

Assigns employees to container. Service validates if employees exists. If employee is not defined then its not assigned to container.

#### Pre conditions

- employess passed in request should be defined

#### Post conditions

- employees are assigned to container
- container's state is changed to ASSIGNED
- if employee is not found in employees collection then its not assigned.

#### Request

```
{
  activityId: String,                         // PK of activity
  containerNumber: String,                    // container number
  assigneeIds: ["employeeId1", "employeeId2"] // list of employees to be assigned to container
}

```

#### Response

- HTTP 200 OK - employees succesfully assigned
- HTTP 400 Illegal Entity if unable to assign
    
### PUT /api/container/{cid}/team/{tid}/assign (Fix: should we introduce the concept of team in the backend??)

Assign an unpacking team to a container

# Incidents

Services for maintaining incidents information - crud operations of pictures of damages linked to containers.

### GET /api/incidents/{unloadingCompanyId}?requestorId={requestorId}&dateFrom={dateFrom}&dateTo={dateTo}

Returns list of incidents created by uploading company for specific creteria. If no more parameters provided
this service will return all incidents across all activities and requestors.

#### Pre conditions

None

#### Post conditions

None

#### Request

- unloadingCompanyId - mandatory, the id of unloading company incidents are inquired for
- requestorId - optional, the id of retail company the incident relates to
- dateFrom - optional, this will narrow the incidents with unloading date on or after this date
- dateTo - optional, this will narrow the incidents with unloading date on or before this date

#### Response

List of incidents or empty list if incidents not found. Data returned: container number, unloading date (incident date),
retail company details, list of images references for further request.


```
{
  "incidents": [
    {
      "containerNumber": "MRKU9727355",
      "incidentDate": "2017-02-06",
      "requestor": {
        "id": "589871b82a19958960fbb892",
        "name": "Kmart",
        "type": "retail",
        "website": "www.kmart.com",
        "email": "contact@kmart.com",
        "phone": "999999"
      },
      "images": [
        "INC1",
        "INC2"
      ]
    }
  ]
}
```

### POST /api/incidents/upload?containerNumber={containerNumber}&file={file}

Service uploads picture file into azure storage, assigns reference to this file and creates incident for container in specific activity document.

#### Pre conditions

- container is defined for activity.
- only one container has specified containerNumber

#### Post conditions

- if container already has incident(s) defined then new file is added to its incidents list.
- if container had no incidents then new incident with single file is defined for its activity.
- service will generate unique reference for received file and return it to caller
- service will store file in azure storage.

#### Request

- containerNumber - required, container number of existing container
- file - image file to be stored.

#### Response

```
{
  reference: String // unique reference for uploaded file
}

```

### GET /api/incidents/reference/{reference}

Service returns picture file for specific file reference.

#### Pre conditions

- incident must exists for activity/container and file with provided reference

#### Request

- reference - file reference as returned by POST /api/incident/upload service

#### Response

- HTTP 200 OK - binaries of file stored in azure storage.
- HTTP 500 internal error - when IO error received from azure
- HTTP 404 not found - when no file identified by reference found

### DELETE /api/incidents/{reference}

Delete picture file from azure storage and removes ot from container incident. If it was last file then whole container incident is also removed.

#### Pre conditions

- incident must exists for activity/container and file with provided reference

#### Post conditions

- file identified by reference has been deleted from azure
- file has been removed from container's incidents array
- if container's incidents array had no files left then container is also removed from incidents array.

#### Request

- reference - file reference as returned by POST /api/incident/upload service

#### Response

- HTTP 200 OK - confirmation message that incident has been deleted
- HTTP 404 not found - when no file identified by reference found

# Resource Assignments

Shipping containers are to be assigned by a team leader to "unloaders" agents in order to commence with the unsealing/unloading activity.
This resource will manage all the operations related with this activity.

### PUT /api/assignments/sites/{siteId}/containers/{containerId}

Assign a container to a group of employees

#### Request

```
    {"employees": ["employeeId", "employeeId"] }
```

### DELETE /api/assignments/sites/{siteId}/containers/{containerId}

It will delete all the assignments to a given container

### GET /api/assignments/sites/{siteId}/employees/{employeeId}?date={date}

Return all the assignments bound to given employee on a given date. if `date` param not specified, today's date is assumed.

### GET /api/assignments/sites/{siteId}/containers/{containerId}?date={date}

Return all the assignments bound to given container on a given date. if `date` param not specified, today's date is assumed.

# Resource Employees

Resource that represent any single user

### GET /api/employees

Returns details of signed on user, User receives full details of itself.

#### Pre condition

- user is logged in to maniflexx.

#### Request

No additional details needed

#### Reposne

HTTP 200 OK:

- ID - unique employee identifier
- fullName - employee's full name
- username - employee's login name
- email 
- phone
- roles - list of roles (string) assigned to employee
- employerId - the id of employer
- employerType - employer's type - unloading, retail

HTTP 404 Not found - authenticated employee not found - this scenario should not happened as attempt to call this service without authentication would result in HTTP 503 not Authorized error.

### GET /api/employees/{username}

Get employee details by username. 

### POST /api/employees

Register a new employee

#### Request

- RequestBody:

```
{
 "fullname": String,
 "username": String,
 "password": String,
 "email": string,
 "phone": string,
 "roleId": id,
 "companyId": id
}
```

#### Response

- ResponseBody
```
{
    "id": long,
    "username": string,
    "fullname": string,
    "roleId" roleId
}
```
- Errors

BAD_REQUEST if username exist or roleId/company does not exist. 

### GET /api/employees/{uid}/containers

Get all the containers assigned to a user

### POST /api/employees/access/reset

Request a temporary access code (sent to the registered email of the user). This code will be sent as a password in the `/sts/issue`. 
Please note that this code will only be valid after one successful login. So please force the user to change the password

#### Request

- RequestBody:
`<username>`

### POST /api/employees/access/change_password

Service changes password for logged in user. oldPassword must match current password. New password must be between 4 and 15 alphanumeric string.

#### Permissions

| Role Names |
| - | - | - | - |
| ROLE_WEB_ADMIN | ROLE_WEB_DASHBOARD | ROLE_ONSITE_UNLOAD | ROLE_ONSITE_TEAML |

#### Request (Body)

```
{
 "oldPassword": string,
 "newPassword": string // (between 4 and 15 alphanumerics of length)
}
```


#### Response

- HTTP 200 (OK)
- HTTP 400 (BAD_REQUEST) if oldPassword does not match or invalid (see length constraints)

### PUT /api/employees/{employeeId}

Updates employee details: full name, username, email, phone and list of roles. For password checking dedicated security services to be used.
Service will overwrite data above with passed information.

#### Request Body

```
{
    "fullname": string,
    "username": string,
    "email": string,
    "phone": string,
    "roles": ["ROLE_WEB_DASHBOARD"]
}
```
#### Response

- HTTP 200 OK
- HTTP 404 NOT_FOUND: employee with id: employeeId not found

# Resource Shippings

This resource represent a shipping manifest, containing a number of container manifest and some
extra metadata.

### GET /api/shipping/{siteid}

Get all shipping information by id

### POST /api/shipping/{shippingId}/notification

Create and attach a new notification to a shipping manifest

### DELETE /api/shipping/{shippingId}/notification/{notificationId}

Remove a notification from a given shipping manifest


# Resource Sites

Physical location linked to a retail company, where unloading companies are assigned to unload containers.

### GET /api/sites

Return all the retail company sites where an unload company (user in the token) has arrangements with.

### GET /api/sites/{id}

Get information about a particular site by siteId

### GET /api/sites/{name}

Get information about a particular site by sitename

### GET /api/sites/{id}/shipping?start=<yyyy-MM-dd>&end=<yyyy-MM-dd>

Get all shipping manifest assigned to a site, in the date range specified. If not dates are specified, it will default
to current local date.

### GET /api/sites/{siteId}/workforce (pending fix: filtering by date is required)

Get the workforce assigned to a site


### GET /api/sites/{siteId}/unloading-events?from=<yyyy-MM-ddTHH:mm:ss>&to=<yyyy-MM-ddTHH:mm:ss>

 Find all unloading events associated with a site, filtered by a range of dates.If dates are not present, it will filtered by today only.

### POST /api/sites/{siteId}/unloading-events

Create and save a new unloading event associating it with a site

#### Request

- RequestBody:

```
{
    "containerId": String,
    "createdByEmployeeId": String,
    "description": String
}
```

# Resource Upload

This abstract resource reprents the uploading capability of shipping manifests into the system via webconsole.

### POST /api/upload

- Request Param: 
  + `clientId`: Retail Company Id, i.e. the owner of the shipping.
  + `file`: The actual shipping manifest.
